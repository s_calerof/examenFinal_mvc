﻿using ExamenFinalMvc.Web.Funcionalidades.EditarFacturas;
using ExamenFinalMvc.Web.Funcionalidades.EliminarFacturas;
using ExamenFinalMvc.Web.Funcionalidades.ListarFacturas;
using ExamenFinalMvc.Web.Funcionalidades.RegistrarFacturas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExamenFinalMvc.Modelo;
using System.IO;

namespace ExamenFinalMvc.Web.Controllers
{
    [Authorize]
    public class FacturasController : Controller
    {
        // GET: Facturas
        public ActionResult Listar(string ruc)
        {
            using (var listarEmpresa = new ListarFacturasHandler())
            {
                return View(listarEmpresa.Ejecutar(ruc));
            }
        }

        [HttpGet]
        public ActionResult Registrar(string ruc)
        {
            return View(new RegistrarFacturaViewModel() { Ruc = ruc });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrar(RegistrarFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var registrarEmpresa = new RegistrarFacturaHandler())
            {
                try
                {

        
                    registrarEmpresa.Ejecutar(modelo);
                    return RedirectToAction("Listar", "Empresas");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }


        [HttpGet]
        public ActionResult Editar(int id)
        {

            using (var buscarFactura = new EditarFacturaHandler())
            {
                return View(buscarFactura.BuscarId(id));
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(EditarFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var editarFactura = new EditarFacturaHandler())
            {
                try
                {
                    editarFactura.Ejecutar(modelo);
                    return RedirectToAction("Listar");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }


        [HttpGet]
        public ActionResult Eliminar(int id)
        {

            using (var buscarFactura = new EliminarFacturaHandler())
            {
                return View(buscarFactura.BuscarId(id));
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Eliminar(EliminarFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var eliminarFactura = new EliminarFacturaHandler())
            {
                try
                {
                    eliminarFactura.Ejecutar(modelo);
                    return RedirectToAction("Listar");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }


      


    }
}