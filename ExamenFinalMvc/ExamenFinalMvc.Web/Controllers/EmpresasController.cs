﻿
using ExamenFinalMvc.Web.Funcionalidades.EditarEmpresas;
using ExamenFinalMvc.Web.Funcionalidades.EliminarEmpresas;
using ExamenFinalMvc.Web.Funcionalidades.ListarEmpresas;
using ExamenFinalMvc.Web.Funcionalidades.RegistrarEmpresas;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

 
namespace ExamenFinalMvc.Web.Controllers
{
    [Authorize]
    public class EmpresasController : Controller
    {
        // GET: Empresas
        public ActionResult Listar()
        {
            //var user = User.Identity.GetUserName();

            using (var listarEmpresa = new ListarEmpresaHandler())
            {
                return View(listarEmpresa.Ejecutar());
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarEmpresaViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registrar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var registrarEmpresa = new RegistrarEmpresaHandler())
            {
                try
                {
                           registrarEmpresa.Ejecutar(modelo);
                    return RedirectToAction("Listar");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }

       

        [HttpGet]
        public ActionResult Editar(int id)
        {

            using (var buscarEmpresa = new EditarEmpresaHandler())
            {
                return View(buscarEmpresa.BuscarId(id));
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(EditarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var editarEmpresa = new EditarEmpresaHandler())
            {
                try
                {
                    editarEmpresa.Ejecutar(modelo);
                    return RedirectToAction("Listar");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }


        [HttpGet]
        public ActionResult Eliminar(int id)
        {

            using (var buscarEmpresa = new EliminarEmpresaHandler())
            {
                return View(buscarEmpresa.BuscarId(id));
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Eliminar(EliminarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var editarEmpresa = new EliminarEmpresaHandler())
            {
                try
                {
                    editarEmpresa.Ejecutar(modelo);
                    return RedirectToAction("Listar");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }


    }
}