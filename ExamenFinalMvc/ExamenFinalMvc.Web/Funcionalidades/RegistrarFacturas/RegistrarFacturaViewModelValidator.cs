﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.RegistrarFacturas
{
    public class RegistrarFacturaViewModelValidator : AbstractValidator<RegistrarFacturaViewModel>
    {
        public RegistrarFacturaViewModelValidator()
        {
            RuleFor(modelo => modelo.FechaEmision)
              .NotEmpty().WithMessage("Fecha de Vencimiento Requerida");

            RuleFor(modelo => modelo.FechaVencimiento)
               .NotEmpty().WithMessage("Fecha de Vencimiento Requerida")
               .Must(ValidaFechaVencimiento).WithMessage("Fecha de vencimiento debe ser mayor o igual a fecha de emision");

            RuleFor(modelo => modelo.FechaCobro)
               .NotEmpty().WithMessage("Fecha de Cobro Requerida")
               .Must(ValidaFechaCobro).WithMessage("Fecha de cobro debe ser mayor o igual a fecha de vencimiento");

            RuleFor(modelo => modelo.Ruc)
              .NotEmpty()
              .Must(ValidaRUC)
              .WithMessage("RUC invalido");

            RuleFor(modelo => modelo.TotalFactura)
                .GreaterThan(0)
                .WithMessage("EL Total Factura Invalido");

            RuleFor(modelo => modelo.TotalIGV)
                .Must(ValidaIGV)
                .WithMessage("IGV invalido");

         }

        private bool ValidaFechaVencimiento(RegistrarFacturaViewModel modelo, DateTime? fechaVencimiento)
        {
            return fechaVencimiento >= modelo.FechaEmision.Value.AddDays(0).Date;

        }

        private bool ValidaFechaCobro(RegistrarFacturaViewModel modelo, DateTime? fechaCobro)
        {
            return fechaCobro >= modelo.FechaVencimiento.Value.AddDays(0).Date;

        }

        private bool ValidaIGV(RegistrarFacturaViewModel modelo, decimal? igv)
        {
            return (igv == (modelo.TotalFactura - (modelo.TotalFactura / Convert.ToDecimal(Math.Round(1.18,2)))));
        }


        public bool ValidaRUC(string identificationDocumentRuc)
        {
            string identificationDocument = identificationDocumentRuc.Trim();

            if ((!string.IsNullOrEmpty(identificationDocument)) && (identificationDocument.Length == 11))
            {
                int addition = 0;
                int[] hash = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                int identificationDocumentLength = identificationDocument.Length;
                string identificationComponent = identificationDocument.Substring(0, identificationDocumentLength - 1);
                int identificationComponentLength = identificationComponent.Length;
                int diff = hash.Length - identificationComponentLength;
                for (int i = identificationComponentLength - 1; i >= 0; i--)
                {
                    addition += (identificationComponent[i] - '0') * hash[i + diff];
                }

                addition = 11 - (addition % 11);

                if (addition == 11)
                {
                    addition = 1;
                }

                if (addition == 10)
                {
                    addition = 0;
                }

                char last = char.ToUpperInvariant(identificationDocument[identificationDocumentLength - 1]);
                if (identificationDocumentLength == 11)
                {
                    return addition.Equals(last - '0');
                }
            }

            return false;
        }




    }
}