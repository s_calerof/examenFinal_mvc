﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.RegistrarFacturas
{
    public class RegistrarFacturaHandler: IDisposable
    {
        private readonly FactoringRepositorio factoringRepositorio;

        public RegistrarFacturaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarFacturaViewModel e)
        {

            Factura factura = new Factura()
            {
                Numero = e.Numero,
                FechaEmision = e.FechaEmision,
                FechaVencimiento = e.FechaVencimiento,
                FechaCobro = e.FechaCobro,
                Ruc = e.Ruc,
                TotalFactura = e.TotalFactura,
                TotalIGV = e.TotalIGV,
                ImagenId = e.ImagenId
            };

            factoringRepositorio.Facturas.Agregar(factura);
            factoringRepositorio.Commit();

        }
    }
}