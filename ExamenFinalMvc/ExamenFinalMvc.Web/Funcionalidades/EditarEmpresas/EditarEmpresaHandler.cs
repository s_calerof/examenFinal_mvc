﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.EditarEmpresas
{
    public class EditarEmpresaHandler : IDisposable
    {
        private readonly FactoringRepositorio factoringRepositorio;

        public EditarEmpresaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public EditarEmpresaViewModel BuscarId(int id)
        {
            var empresa = factoringRepositorio.Empresas.TraerUno(x => x.Id == id);
            return new EditarEmpresaViewModel()
            {
                Id = empresa.Id,
                RUC = empresa.RUC,
                RazonSocial = empresa.RazonSocial,
                Direccion = empresa.Direccion,
                Departamento = empresa.Departamento,
                Provincia = empresa.Provincia,
                Distrito = empresa.Distrito,
                Rubro = empresa.Rubro,
                UserId = empresa.UserId
            };

        }


        public void Ejecutar(EditarEmpresaViewModel e)
        {

            Empresa empresa = new Empresa()
            {
                Id = e.Id,
                RUC = e.RUC,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                UserId = e.UserId
            };

            factoringRepositorio.Empresas.Actualizar(empresa);
            factoringRepositorio.Commit();
        }


    }
}