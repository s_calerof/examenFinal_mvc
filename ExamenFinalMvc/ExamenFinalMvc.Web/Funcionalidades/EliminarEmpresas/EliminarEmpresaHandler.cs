﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.EliminarEmpresas
{
    public class EliminarEmpresaHandler: IDisposable
    {

        private readonly FactoringRepositorio factoringRepositorio;

        public EliminarEmpresaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public EliminarEmpresaViewModel BuscarId(int id)
        {
            var empresa = factoringRepositorio.Empresas.TraerUno(x => x.Id == id);
            return new EliminarEmpresaViewModel()
            {
                Id = empresa.Id,
                RUC = empresa.RUC,
                RazonSocial = empresa.RazonSocial,
                Direccion = empresa.Direccion,
                Departamento = empresa.Departamento,
                Provincia = empresa.Provincia,
                Distrito = empresa.Distrito,
                Rubro = empresa.Rubro,
                UserId = empresa.UserId
            };

        }


        public void Ejecutar(EliminarEmpresaViewModel e)
        {

            Empresa empresa = factoringRepositorio.Empresas.TraerUno(x => x.Id == e.Id);
            Factura factura = factoringRepositorio.Facturas.TraerUno(x => x.Ruc == empresa.RUC);

            if (factura != null)
            {
                factoringRepositorio.Empresas.Eliminar(empresa);
                factoringRepositorio.Commit();
            }

        }
    }
}