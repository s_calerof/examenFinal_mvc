﻿using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenFinalMvc.Web.Funcionalidades.RegistrarEmpresas
{
    [Validator(typeof(RegistrarEmpresaViewModelValidator))]
    public class RegistrarEmpresaViewModel
    {
        public string RUC { get; set; }

        public string RazonSocial { get; set; }

        public string Direccion { get; set; }

        public string Departamento { get; set; }

        public string Provincia { get; set; }

        public string Distrito { get; set; }

        public string Rubro { get; set; }

        public string UserId { get; set; }

        public SelectList Departamentos { get; set; }
        public SelectList Provincias { get; set; }
        public SelectList Distritos { get; set; }


        public RegistrarEmpresaViewModel()
        {
            var listaVacia = new List<string>() { "Seleccione..." };
            Departamentos = new SelectList(listaVacia);
            Provincias = new SelectList(listaVacia);
            Distritos = new SelectList(listaVacia);
        }


    }

}