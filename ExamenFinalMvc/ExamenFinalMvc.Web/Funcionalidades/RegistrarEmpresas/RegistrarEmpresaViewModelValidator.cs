﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.RegistrarEmpresas
{
    public class RegistrarEmpresaViewModelValidator : AbstractValidator<RegistrarEmpresaViewModel>
    {

        public RegistrarEmpresaViewModelValidator()
        {
            RuleFor(modelo => modelo.RazonSocial)
               .NotEmpty()
               .Length(4, 250)
               .WithMessage("Razon Social debe tener una longitud entre 4 a 250 carateres");

            RuleFor(modelo => modelo.RUC)
             .NotEmpty()
             .Must(ValidaRUC)
             .WithMessage("RUC invalido");

            RuleFor(modelo => modelo.Rubro)
               .NotEmpty()
               .Length(4, 250)
               .Matches(@"^[a-zA-Z-']*$")
               .WithMessage("Rubro debe contener solo letras y una longitud entre 4 a 250 carateres");


        }

        public bool ValidaRUC(string identificationDocumentRuc)
        {
            string identificationDocument = identificationDocumentRuc.Trim();

            if ((!string.IsNullOrEmpty(identificationDocument)) && (identificationDocument.Length == 11))
            {
                int addition = 0;
                int[] hash = { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 };
                int identificationDocumentLength = identificationDocument.Length;
                string identificationComponent = identificationDocument.Substring(0, identificationDocumentLength - 1);
                int identificationComponentLength = identificationComponent.Length;
                int diff = hash.Length - identificationComponentLength;
                for (int i = identificationComponentLength - 1; i >= 0; i--)
                {
                    addition += (identificationComponent[i] - '0') * hash[i + diff];
                }

                addition = 11 - (addition % 11);

                if (addition == 11)
                {
                    addition = 1;
                }

                if (addition == 10)
                {
                    addition = 0;
                }

                char last = char.ToUpperInvariant(identificationDocument[identificationDocumentLength - 1]);
                if (identificationDocumentLength == 11)
                {
                    return addition.Equals(last - '0');
                }
            }

            return false;
        }







    }
}