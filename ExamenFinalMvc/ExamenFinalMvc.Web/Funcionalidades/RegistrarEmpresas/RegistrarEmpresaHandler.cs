﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.RegistrarEmpresas
{
    public class RegistrarEmpresaHandler : IDisposable
    {
        private readonly FactoringRepositorio factoringRepositorio;

        public RegistrarEmpresaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarEmpresaViewModel e)
        {

            Empresa empresa = new Empresa()
            {

                RUC = e.RUC,
                RazonSocial = e.RazonSocial,
                Direccion = e.Direccion,
                Departamento = e.Departamento,
                Provincia = e.Provincia,
                Distrito = e.Distrito,
                Rubro = e.Rubro,
                UserId = e.UserId
            };

            factoringRepositorio.Empresas.Agregar(empresa);
            factoringRepositorio.Commit();

        }

    }
}