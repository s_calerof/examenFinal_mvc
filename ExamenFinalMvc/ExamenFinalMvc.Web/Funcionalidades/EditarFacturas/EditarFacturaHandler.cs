﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.EditarFacturas
{
    public class EditarFacturaHandler : IDisposable
    {
        private readonly FactoringRepositorio factoringRepositorio;

        public EditarFacturaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public EditarFacturaViewModel BuscarId(int id)
        {
            var e = factoringRepositorio.Facturas.TraerUno(x => x.Id == id);
            return new EditarFacturaViewModel()
            {
                Id = e.Id,
                Numero = e.Numero,
                FechaEmision = e.FechaEmision,
                FechaVencimiento = e.FechaVencimiento,
                FechaCobro = e.FechaCobro,
                Ruc = e.Ruc,
                TotalFactura = e.TotalFactura,
                TotalIGV = e.TotalIGV,
                ImagenId = e.ImagenId
            };

        }


        public void Ejecutar(EditarFacturaViewModel e)
        {

            Factura factura = new Factura()
            {
                Id = e.Id,
                Numero = e.Numero,
                FechaEmision = e.FechaEmision,
                FechaVencimiento = e.FechaVencimiento,
                FechaCobro = e.FechaCobro,
                Ruc = e.Ruc,
                TotalFactura = e.TotalFactura,
                TotalIGV = e.TotalIGV,
                ImagenId = e.ImagenId
            };

            factoringRepositorio.Facturas.Actualizar(factura);
            factoringRepositorio.Commit();
        }
    }
}