﻿using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.ListarFacturas
{
    
    public class ListarFacturasHandler: IDisposable
    {

        private readonly FactoringRepositorio factoringRepositorio;

        public ListarFacturasHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public IEnumerable<ListarFacturasViewModel> Ejecutar(string ruc)
        {

            return factoringRepositorio.Facturas.TrerTodos().Where(x=> x.Ruc == ruc)
                .Select(e =>
                    new ListarFacturasViewModel()
                    {
                        Id = e.Id,
                        Numero = e.Numero,
                        FechaEmision = e.FechaEmision,
                        FechaVencimiento = e.FechaVencimiento,
                        FechaCobro = e.FechaCobro,
                        Ruc = e.Ruc,
                        TotalFactura = e.TotalFactura,
                        TotalIGV = e.TotalIGV,
                        ImagenId = e.ImagenId
                    }
                ).ToList();

        }


    }
}