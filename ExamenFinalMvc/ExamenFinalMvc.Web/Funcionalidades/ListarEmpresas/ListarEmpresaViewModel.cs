﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.ListarEmpresas
{
    public class ListarEmpresaViewModel
    {
        public int Id { get; set; }

        public string RUC { get; set; }

        public string RazonSocial { get; set; }

        public string Direccion { get; set; }

        public string Departamento { get; set; }

        public string Provincia { get; set; }

        public string Distrito { get; set; }

        public string Rubro { get; set; }

        public string UserId { get; set; }

    }
}