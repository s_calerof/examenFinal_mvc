﻿using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.ListarEmpresas
{
    public class ListarEmpresaHandler : IDisposable
    {

        private readonly FactoringRepositorio factoringRepositorio;

        public ListarEmpresaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public IEnumerable<ListarEmpresaViewModel> Ejecutar()
        {

            return factoringRepositorio.Empresas.TrerTodos()
                .Select(e =>
                    new ListarEmpresaViewModel()
                    {
                        Id = e.Id,
                        RUC = e.RUC,
                        RazonSocial = e.RazonSocial,
                        Direccion = e.Direccion,
                        Departamento = e.Departamento,
                        Provincia = e.Provincia,
                        Distrito = e.Distrito,
                        Rubro = e.Rubro,
                        UserId = e.UserId
                    }
                ).ToList();

        }




    }
}