﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.EliminarFacturas
{
    public class EliminarFacturaViewModel
    {
        public int Id { get; set; }

        public string Numero { get; set; }

        public DateTime? FechaEmision { get; set; }

        public DateTime? FechaVencimiento { get; set; }

        public DateTime? FechaCobro { get; set; }

        public string Ruc { get; set; }

        public decimal? TotalFactura { get; set; }

        public decimal? TotalIGV { get; set; }

        public int? ImagenId { get; set; }
    }
}