﻿using ExamenFinalMvc.Modelo;
using ExamenFinalMvc.Repositorio;
using ExamenFinalMvc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamenFinalMvc.Web.Funcionalidades.EliminarFacturas
{
    public class EliminarFacturaHandler : IDisposable
    {
        private readonly FactoringRepositorio factoringRepositorio;

        public EliminarFacturaHandler()
        {
            factoringRepositorio = new FactoringRepositorio(new FactoringContexto());
        }

        public void Dispose()
        {
            factoringRepositorio.Dispose();
        }

        public EliminarFacturaViewModel BuscarId(int id)
        {
            var e = factoringRepositorio.Facturas.TraerUno(x => x.Id == id);
            return new EliminarFacturaViewModel()
            {
                Id = e.Id,
                Numero = e.Numero,
                FechaEmision = e.FechaEmision,
                FechaVencimiento = e.FechaVencimiento,
                FechaCobro = e.FechaCobro,
                Ruc = e.Ruc,
                TotalFactura = e.TotalFactura,
                TotalIGV = e.TotalIGV,
                ImagenId = e.ImagenId
            };

        }


        public void Ejecutar(EliminarFacturaViewModel e)
        {

            Factura  factura = factoringRepositorio.Facturas.TraerUno(x => x.Id == e.Id);
            factoringRepositorio.Facturas.Eliminar(factura);
            factoringRepositorio.Commit();
        }

    }
}