namespace ExamenFinalMvc.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

       
        [StringLength(12)]
        public string Numero { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaEmision { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaVencimiento { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaCobro { get; set; }

        [StringLength(11)]
        public string Ruc { get; set; }

        public decimal? TotalFactura { get; set; }

        public decimal? TotalIGV { get; set; }

        public int? ImagenId { get; set; }
    }
}
