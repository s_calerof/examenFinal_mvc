namespace ExamenFinalMvc.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.IO;

    [Table("FacturaImagen")]
    public partial class FacturaImagen
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(100)]
        public string Titulo { get; set; }

        [StringLength(10)]
        public string Extension { get; set; }

        public byte[] ImagenData { get; set; }

       
    }
}
