namespace ExamenFinalMvc.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(11)]
        public string RUC { get; set; }

        [Required]
        [StringLength(250)]
        public string RazonSocial { get; set; }

        [StringLength(50)]
        public string Direccion { get; set; }

        [StringLength(50)]
        public string Departamento { get; set; }

        [StringLength(50)]
        public string Provincia { get; set; }

        [StringLength(50)]
        public string Distrito { get; set; }

        [StringLength(250)]
        public string Rubro { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }
    }
}
