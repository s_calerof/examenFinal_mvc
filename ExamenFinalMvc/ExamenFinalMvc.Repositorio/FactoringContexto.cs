namespace ExamenFinalMvc.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Modelo;

    public partial class FactoringContexto : DbContext
    {
        public FactoringContexto()
            : base("name=Factoring")
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<FacturaImagen> FacturaImagen { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>()
                .Property(e => e.RUC)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Provincia)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Distrito)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Rubro)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.Ruc)
                .IsUnicode(false);

            modelBuilder.Entity<FacturaImagen>()
                .Property(e => e.Titulo)
                .IsUnicode(false);

            modelBuilder.Entity<FacturaImagen>()
                .Property(e => e.Extension)
                .IsUnicode(false);
        }
    }
}
