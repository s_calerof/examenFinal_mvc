﻿using ExamenFinalMvc.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenFinalMvc.Repositorio.Impl
{
    public class FactoringRepositorio : IDisposable
    {

        private readonly DbContext bd;

        private readonly RepositorioGenerico<Factura> _factura;
        private readonly RepositorioGenerico<Empresa> _empresa;
        private readonly RepositorioGenerico<FacturaImagen> _facturaImagen;

        public FactoringRepositorio(DbContext bd)
        {
            this.bd = bd;
            _factura = new RepositorioGenerico<Factura>(bd);
            _empresa = new RepositorioGenerico<Empresa>(bd);
            _facturaImagen = new RepositorioGenerico<FacturaImagen>(bd);
        }

        public RepositorioGenerico<Factura> Facturas { get { return _factura; } }
        public RepositorioGenerico<Empresa> Empresas { get { return _empresa; } }
        public RepositorioGenerico<FacturaImagen> FacturaImagenes { get { return _facturaImagen; } }

        public void Commit()
        {
            bd.SaveChanges();
        }

        public void Dispose()
        {
            bd.Dispose();
        }
    }
}
