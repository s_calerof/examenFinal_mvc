namespace ExamenFinalMvc.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RUC = c.String(nullable: false, maxLength: 11, unicode: false),
                        RazonSocial = c.String(nullable: false, maxLength: 250, unicode: false),
                        Direccion = c.String(maxLength: 50, unicode: false),
                        Departamento = c.String(maxLength: 50, unicode: false),
                        Provincia = c.String(maxLength: 50, unicode: false),
                        Distrito = c.String(maxLength: 50, unicode: false),
                        Rubro = c.String(maxLength: 250, unicode: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(maxLength: 12, unicode: false),
                        FechaEmision = c.DateTime(storeType: "date"),
                        FechaVencimiento = c.DateTime(storeType: "date"),
                        FechaCobro = c.DateTime(storeType: "date"),
                        Ruc = c.String(maxLength: 11, unicode: false),
                        TotalFactura = c.Decimal(precision: 18, scale: 2),
                        TotalIGV = c.Decimal(precision: 18, scale: 2),
                        ImagenId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FacturaImagen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(maxLength: 100, unicode: false),
                        Extension = c.String(maxLength: 10, unicode: false),
                        ImagenData = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FacturaImagen");
            DropTable("dbo.Factura");
            DropTable("dbo.Empresa");
        }
    }
}
